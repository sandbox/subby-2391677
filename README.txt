-- SUMMARY --

The language url selection module registers a new ctools access plugin 
that creates a domain language selection rule.

For a full description of the module, visit the project page:
  http://drupal.org/project/language_url_selection_rule

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/language_url_selection_rule


-- REQUIREMENTS --

Drupal 7
ctools


-- INSTALLATION --

* Install as usual.

* Setup your language domains ... if you haven't done that already.


-- CONTACT --

Current maintainers:
* Fredi Bach (Subby) - http://drupal.org/user/2738721

This project has been sponsored by:
* getunik
  Specialized in NGO clients. Located in Zurich, Switzerland.