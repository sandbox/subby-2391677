<?php

/**
 * @file
 * Language url slection rule ctools access plugin code
 *
 * Ctools plugin that adds a new selection rule for language domains.
 */


/**
 * Ctools plugin settings:
 */
$plugin = array(
  'title'             => t('String: Language-URL prefix'),
  'description'       => t('Craetes a language rule based on the domain or url language and not the node language'),
  'callback'          => 'language_url_selection_rule_language_value_ctools_access_check',
  'default'           => array('language_value' => ''),
  'settings form'     => 'language_url_selection_rule_language_value_ctools_settings',
  'summary'           => 'language_url_selection_rule_language_value_ctools_summary',
  'required context'  => new ctools_context_required(t('Node'), 'node')
);



/**
 * Implements hook_ctools_access_check().
 */
function language_url_selection_rule_language_value_ctools_access_check($conf, $context) {

  global $language_url;

  if (empty($context) || empty($context->data) || empty($conf['language_value'])) {
    return FALSE;
  }
  
  if ($conf['language_value'] == $language_url->language){
	  return TRUE;
  }
  
  return FALSE;

}


/**
 * Implements hook_ctools_settings().
 */
function language_url_selection_rule_language_value_ctools_settings($form, &$form_state, $conf) {

  $form['settings']['language_value'] = array(
    '#type'             => 'textfield',
    '#title'            => t('Language Domain'),
    '#description'      => t('Only display if the language prefix is being matched by the domains language, for example "en".'),
    '#default_value'    => $conf['language_value']
  );

  return $form;

}


/**
 * Implements hook_ctools_summary().
 */
function language_url_selection_rule_language_value_ctools_summary($conf, $context) {

  return t('Nodes with language-url prefix "@lang"', array('@lang' => $conf['language_value']));

}
